
<!DOCTYPE html>
<html>
<head>
  <title> Détail</title>
  <link rel="stylesheet" type="text/css" href="css/animation.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="icon" href="favicon.ico"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!-- header -->
<?php include "header.php" ?>
<center>
	


 <div style="width: 500px;">
 	
 	<form action="addsujet.php" method="post">

  <div class="form-group">
    <label for="site">Société/Site</label>
    <input type="text" class="form-control" id="site" name ="site" aria-describedby="emailHelp" placeholder=" Ex : Safran/Dijon">
    
  </div>


<div class="form-group">
    <label for="Sujet">Sujet</label>
    <input type="text" class="form-control" id="sujet" name ="sujet" aria-describedby="emailHelp" placeholder=" Ex : Expédition de marchandise">

  </div>

  <div class="form-group">
    <label for="typeprojet">Type du projet</label>
   <select class="form-control" id="typeprojet" name="typeprojet">
      <option>Projet Ponctuel</option>
      <option>Projet Numérique</option>
      <option>Projet de Démarrage</option>
      <option>Projet Amélioration Continue</option>
      <option>Projet Innovation</option>
    </select>

  </div>

  <div class="form-group">
    <label for="pilote">Pilote du projet</label>
    <input type="text" class="form-control" id="pilote" name ="pilote" aria-describedby="emailHelp" placeholder=" Ex : Xavier Dalis">

  </div>

<div class="form-group">
    <label for="startday">Date de Début</label>
    <input type="date" class="form-control" id="startday" name ="startday" aria-describedby="emailHelp" placeholder=" Ex : 25/02/1999">

  </div>

  <div class="form-group">
    <label for="enday">Date de Fin (prévue)</label>
    <input type="date" class="form-control" id="enday" name ="enday" aria-describedby="emailHelp" placeholder=" Ex : 04/03/1999">

  </div>





  <button type="submit" style="width: 100%" class="btn btn-primary">Ajouter</button>
</form>

 </div>
</center>

</body>
</html>