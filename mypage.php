
<!DOCTYPE html>
<html>
<head>
  <title>Mes Projets</title>
  <link rel="stylesheet" type="text/css" href="css/animation.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="icon" href="favicon.ico"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!-- header -->
<?php include "header.php" ?>



<table class="table table-bordered table-hover" style="margin-top: 50px">
  <thead>
    <tr style="text-align: center;">
      
         <th scope="col">Société/Site</th>
      
      <th scope="col">Type du projet</th>
   <!-- <th scope="col">Début</th> -->
      <th scope="col">Sujet</th>
     
      <th scope="col">Avancement</th>
     
    
      
       <th scope="col">Pilote</th>

        <th scope="col">Détail</th>

       <th scope="col">MAJ</th>
  		<th scope="col">Supprimer</th>
    </tr>
  </thead>

  <?php 
include('serveur.php');

$query = "SELECT * FROM sujet";
$result = $pdo -> query($query);
while($donne=$result->fetch()){

 
   ?>


  <tbody>
   <tr style="text-align: center;">
     
        <td><?php echo $donne['site']; ?></td>
        <td><?php echo $donne['typeprojet']; ?></td>
      <!-- <td><?php echo $donne['startday']; ?></td> -->
       
    
      <td><?php echo $donne['sujet']; ?></td>
      <td><?php echo $donne['avancement']; ?>%</td>

       <td><?php echo $donne['pilote']; ?></td>
      <td>

			<form method="post" action="detail.php">
				
				<button type="submit" name = "idprojet" value="<?php echo $donne['identifiant']; ?>" class="btn btn-info">+</button>

			</form>
			      	

      </td>


      


      
        <td>

        	<form method="post" action="MAJ.php">
				
				<button type="submit" name = "maj" value="<?php echo $donne['identifiant']; ?>" class="btn btn-warning">Mise à jour</button>

			</form>

		</td>



			<td>


			<form method="post" action="deletsujet.php">
				
				<button type="submit" name = "delet" value="<?php echo $donne['identifiant']; ?>" class="btn btn-danger">Supprimer</button>

			</form>
      	


      </td>
     
    </tr>



     <?php
}
?>
  </tbody>
</table>



</body>
</html>